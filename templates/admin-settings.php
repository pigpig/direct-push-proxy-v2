<style>
table td, table th {
	padding: 5px;
	word-break: break-all;
}
</style>
<script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>">
document.addEventListener('DOMContentLoaded', function(){
	console.log("Keepalive: "+<?php echo $_['keepalive']; ?>);
	document.getElementById("keepalive").value = <?php echo $_['keepalive']; ?>;
	document.getElementById("setKeepaliveBtn").addEventListener("click", setKeepalive);
});
function setKeepalive(){
	console.log("setKeepalive run...");
	if (document.getElementById("keepalive").value.length > 0){
		console.log(document.getElementById("keepalive").value);

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				location.reload();
			}
		};
		xhttp.open("GET", "../../index.php/apps/ssepush/setkeepalive/"+document.getElementById("keepalive").value, true);
		xhttp.send();
	}
}
</script>
<div id="ssepush-admin" class="section">
<h2>Administration and troubleshooting</h2>
<p class="settings-hint">set up the service and solve common problems.</p>
<p>This has been set up with the objective of making installation and configuration as painless as possible, however, there are certain requirements that must be met in order for the system to perform correctly.</p>
<br><div>
<b>1)</b> This service required Redis to be installed and configured for use by Nextcloud. The database ID used will be 1 higher than the database ID used by Nextcloud.<br>
<b>2)</b> Maximum PHP processes must be set sufficiently high. Each user connected for push messaging will require their own PHP process running, therefore you must set the process limits sufficiently high in order to serve not only the needs of all push clients, but other web clients as well. If you anticipate a maximum of 200 push clients to be connected simultaneously and require an additional 100 servers for web traffic, then you should set your process limit to at least 300. The parameter to adjust in your php-fpm configuration is <b>pm.max_children</b>.<br>
<br><h2>If a proxy is in use, including access to PHP being by way of fastCGI...</h2>
<b>3)</b> Elimination of buffering. Most web server stacks can be instructed to flush buffers by issuing a flush() in php, however, this most notably does not work when using php-fpm. This buffering can result in pushed messages being delayed. In order for messages to be delivered instantaneously, it is necessary to eliminate this additional buffering. In Apache, this can be done by adding the below to your php configuration;<br>
<b>
&lt;Proxy "fcgi://localhost/" disablereuse=on flushpackets=on max=10&gt;<br>
&lt;/Proxy&gt;<br></b><br>
<b>4)</b> Web server proxy timeout should be set to at least 10 minutes. Connection to PHP will typically be by way of fastCGI, which uses the proxy infrastructure of your web server. If you observe clients frequently reconnecting, typically at 1 minute intervals, it means that the proxy is timing out before the keep-alive message is issued. Default keep-alive's are issued at 300 second (5 minute) intervals. In Apache, you can add <b>ProxyTimeout 600</b> to your Nextcloud virtual host.<br>
<br>If it isn't possible to set the proxy timeout to a sensible amount of time, an alternative is to reduce the time interval between keep-alive messages so that they are issued within the available timeout, such as 55 seconds. This may cause an increase in power consumption for the mobile device.
<div><input type="text" id="keepalive" placeholder="Keep-alive interval">
<button class="button" id="setKeepaliveBtn">Set keep-alive</button>
</div>
</div>
<br>
<br>
<p><b>Q:</b> Somebody told me that php is a huge memory hog and uses 50 MB per process. Won't having hundreds or thousands of processes running cripple my server?</p><br>
<p><b>A:</b> Somebody was misinformed about how memory is reported. Memory reporting can be quite complicated, because the memory tied to a process isn't just the memory taken by the process itself, but of all of the different shared libraries that the process links to. So while each php process may be reporting 50 MB, the vast majority of that is the same memory being separately reported by all of the different processes.</p><br>
<p><b>Q:</b> Somebody also told me that php isn't suitable for long running processes because everybody else uses it for short processes that die almost immediately.</p><br>
<p><b>A:</b> The "because everybody else does" argument is a fallacy. Somebody probably also said something about memory leaks accumulating over time. Sure, your typical website designer isn't going to be well educated and competent when it comes to good programming practices, and this increases the chances that code they write will leak memory. A very poor workaround for code that leaks memory is to frequently kill and restart the process. Everybody.... who doesn't know what they're doing.... does it. Even more importantly than the length of time that a process is running, is the amount of work that it does. And while this is a long running process, it is mostly just idle and waiting for input. Even if the code did leak memory, there wouldn't be much opportunity for it to become a problem since idle processes don't leak memory.</p><br>

</div>
