<style>
table td, table th {
	padding: 5px;
	word-break: break-all;
}
</style>
<script nonce="<?php p(\OC::$server->getContentSecurityPolicyNonceManager()->getNonce()) ?>">
document.addEventListener('DOMContentLoaded', function(){
	document.getElementById("newTokenBtn").addEventListener("click", newToken);
<?php	
$items = $_['parameters'];
foreach($items as $row)
	echo "\tdocument.getElementById(\"del-".$row['token']."\").addEventListener(\"click\", delToken);\n";
?>
});
function newToken(){
	console.log("newToken run...");
	if (document.getElementById("newServiceName").value.length > 0){
		console.log(document.getElementById("newServiceName").value);

		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				location.reload();
			}
		};
		xhttp.open("GET", "../../index.php/apps/ssepush/genauth/"+document.getElementById("newServiceName").value, true);
		xhttp.send();
	}
}
function delToken(event){
	console.log("deltoken..."+event.target.id.split("-")[1]);
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			location.reload();
		}
	};
	xhttp.open("GET", "../../index.php/apps/ssepush/rmauth/"+event.target.id.split("-")[1], true);
	xhttp.send();
}
</script>
<div id="ssepush-inst" class="section">
<h2>Mobile push messaging</h2>
<p class="settings-hint">freedom respecting message pushing for your mobile.</p>
<p>In order for certain Nextcloud based applications to work (like talk/spreed in particular), they depend on instantaneous delivery of notifications to the user's mobile device. The way that Nextcloud has this implemented requires the use of TWO separate 3rd party services. The first is a service provided by Nextcloud at https://push-notifications.nextcloud.com. The second is FCM provided by Google.</p>
<br><p>This introduces a couple of problems;</p>
<div><br>
<b>1)</b> While the messages are encrypted until they reach your phone, both services are aware that a message has been transferred.<br>
<b>2)</b> For those opting not to have proprietary Google services installed on their phone, or obtain the Nextcloud applications from alternative sources like F-Droid, FCM is not available, and messages can't be delivered.<br>
</div><br>
<p>This service, along with the companion mobile application, allows messages to be delivered from this Nextcloud instance directly to your mobile device, without the involvement of ANY 3rd party services. In addition, other Nextcloud instances can push messages through this instance along the same connection. And finally, 3rd party messaging sources can be configured to push messages to your mobile device through this Nextcloud instance.</p>
<br>
<p>The companion application source code and issue tracker can be found here;<br>
<a style="color: blue; text-decoration: underline;" href="https://gitlab.com/Nextcloud-Push/nextcloud-push-notifier">https://gitlab.com/Nextcloud-Push/nextcloud-push-notifier</a></p>
<br>
<p>For security purposes, it is highly recommended to build software from source, however, if this is not an option, use the following;</p>
<br>
<a href="https://gitlab.com/Nextcloud-Push/nextcloud-push-notifier/uploads/39a432f3a1fd6de394caf86411ff4328/PushNotifier-v0.0.6.apk" download="PushNotifier-v0.0.6.apk"><button class="button">Install from GitLab Release</button></a>
<br>
<br>
<p>The mobile application depends on the main Nextcloud (files) application, which can be installed from any source, including F-Droid. Add all of the Nextcloud accounts for which you wish to receive push notifications. The LAST account entered will be marked as the MASTER instance with a "*", and must have this plug-in installed. If you enter the accounts in the wrong order, simply re-add the master account last.<br>
<br>After all accounts have been entered, press "[RE]LOAD SERVICE" to activate. It will be automatically start on boot subsequently.<br>
<br>The application uses a foreground service with a persistent notification to avoid being evicted from memory. The notification can be hidden without impacting the behavior. Simply long-click the notification, press the Settings button (gear icon), and uncheck the switch beside "Nextcloud Service".<br>
<br>
</div>
<div id="ssepush-auth" class="section">
<h2>Notifications for third party services</h2>
<p class="settings-hint">configuration for third party services to push messages to your mobile device.</p>

<table cellpadding=3 id="app-tokens-table">
<thead>
<tr>
<th></th>
<th><b>Service</b></th>
<th><b>Authorization Token</b></th>
</tr>
</thead>

<tbody>
<?php
$items = $_['parameters'];
foreach($items as $row){
	echo "<tr><td><button id=\"del-".$row['token']."\" class=\"button\">DEL</button></td><td>".$row['ext_name']."</td><td style=\"column-width: 50px;\">".$row['token']."</td></tr>\n";
}
?>
</tbody>
</table>

<div><input type="text" id="newServiceName" placeholder="Service name">
<button class="button" id="newTokenBtn">Create new authorization</button>
</div>

<br>
<div>
Third party services may push messages to this nextcloud instance by POST'ing to the following URI with the parameters specified below;
<br>
<b><?php echo \OC::$server->getURLGenerator()->getBaseUrl()."/index.php/apps/ssepush/external"; ?></b>
<br>
<br>
<table cellpadding=3>
<thead>
<tr>
<th><b>Parameter</b></th>
<th><b>Required</b></th>
<th><b>Description</b></th>
</tr>
</thead>
<tbody>
<tr>
<th>auth</th>
<th>yes</th>
<th>The Authorization Token from the list above.</th>
</tr>
<tr>
<th>id</th>
<th>yes</th>
<th>This is an identifier unique to the 3rd party service.</th>
</tr>
<tr>
<th>subject</th>
<th>yes</th>
<th>Message subject line.</th>
</tr>
<tr>
<th>message</th>
<th>yes</th>
<th>Message contents.</th>
</tr>
<tr>
<th>type</th>
<th>no</th>
<th>A message type for 3rd party service. I.e. "call", "message".</th>
</tr>
<tr>
<th>sparams</th>
<th>no</th>
<th>JSON-encoded parameters for the message subject.</th>
</tr>
<tr>
<th>mparams</th>
<th>no</th>
<th>JSON-encoded parameters for the message.</th>
</tr>
<tr>
<th>link</th>
<th>no</th>
<th>Used to specify a notification ACTION.</th>
</tr>
<tr>
<th>action[x]</th>
<th>no</th>
<th>Array of JSON-encoded actions for notification action buttons.</th>
</tr>
</tbody>
</table>
<br>
The format for each element in action[] will be {"label":"some label","link":"some link","method","some method"}
</div>

</div>
