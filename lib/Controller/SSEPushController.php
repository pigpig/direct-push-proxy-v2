<?php
declare(strict_types=1);

namespace OCA\SSEPush\Controller;

use OC_Util;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataDisplayResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\IConfig;
use OCP\IDBConnection;
use OCP\IRequest;
use OCP\IUserSession;
use Redis;
use DateTime;

class SSEPushController extends Controller {

	private $db;
	private $config;
	private $userSession;

	public function __construct(IConfig $config, IDBConnection $db, IUserSession $userSession){
		$this->config = $config;
		$this->db = $db;
		$this->userSession = $userSession;
	}

	function err_die($message){
		error_log("Nextcloud/SSEPush: $message");
		exit(1);
	}

	function redis_connect(){
		$redis_db = 1;

		if (!class_exists('Redis')) $this->err_die("Redis is not installed.");

		$redis_config = $this->config->getSystemValue("redis");
		if (!is_array($redis_config)) $this->err_die("Redis is not configured in nextcloud config.php.");
		if (array_key_exists("dbindex", $redis_config)) $redis_db = $redis_config['dbindex'] + 1;

		$redis = new Redis();
		$redis->connect($redis_config['host'], $redis_config['port']) or $this->err_die ("Redis connect error.");
		if (array_key_exists('password', $redis_config)) $redis->auth($redis_config['password']);
		$redis->select($redis_db) or $this->err_die ("Redis database select error.");

		return $redis;
	}

	/**
	 * Push message to mobile client.
	 *
	 * @NoCSRFRequired
	 * @PublicPage
	 *
	 * @param array $notifications
	 * @return DataResponse
	 */
	public function notify(array $notifications){

		$redis = $this->redis_connect();

		foreach ($notifications as $message){
			$devblock = json_decode($message, true);
			if (!isset($devblock['deviceIdentifier']) || !isset($devblock['pushTokenHash']))
				continue;

			if ($redis->get('auth_'.$devblock['pushTokenHash'].'_'.$devblock['deviceIdentifier']) === false){
				return new DataDisplayResponse('{"unknown": ["'.$devblock['deviceIdentifier'].'"], "failed": 0}',
					Http::STATUS_OK,
					['Content-Type' => 'application/json']
				);
			}

			$redis->lPush($devblock['pushTokenHash'].'_'.$devblock['deviceIdentifier'], $message);
		}

		return new DataDisplayResponse('{"unknown": [], "failed": 0}', Http::STATUS_OK, [
			'Content-Type' => 'application/json',
		]);
	}

	/**
	 * Subscribe to SSE push messaging.
	 *
	 * @NoCSRFRequired
	 * @PublicPage
	 *
	 * @param string $deviceIdentifiers
	 * @param string $pushTokenHash
	 */
	public function ssesub(string $deviceIdentifiers, string $pushTokenHash){
		ini_set("default_socket_timeout", "600");
		set_time_limit(0);

		$redis = $this->redis_connect();

		$keys = array();
		$deviceIdentifiers = explode(",", $deviceIdentifiers);
		if (count($deviceIdentifiers) < 1) return new JSONResponse(['success' => false], Http::STATUS_UNAUTHORIZED);
		$user = "";
		foreach($deviceIdentifiers as $value){
			$key = $pushTokenHash.'_'.$value;
			$user = $redis->get('auth_'.$key);
			if ($user === false){
				return new JSONResponse(['success' => false], Http::STATUS_UNAUTHORIZED);
			}
			$keys[] = $key;
			$redis->rPush($key, "shutdown_".getmypid());
		}

		// Record this sub in redis, but expire after 12 hours (43200 seconds)
		$timestamp = (new DateTime())->getTimestamp();
		$redis->setEx('subscribe_'.$user.'_'.$pushTokenHash.'_'.$timestamp, 43200, $timestamp);
		$subcount = count($redis->keys('subscribe_'.$user.'_'.$pushTokenHash.'_*'));

		OC_Util::obEnd();
		header('Cache-Control: no-cache');
		header('X-Accel-Buffering: no');
		header('Content-Type: text/event-stream');
		flush();

		echo "event: start".PHP_EOL;
		echo "data: start".PHP_EOL;
		echo PHP_EOL;
		flush();

		if ($subcount > 144){
			echo "event: warning".PHP_EOL;
			echo "data: reconnected $subcount times in 12 hours".PHP_EOL;
			echo PHP_EOL;
			flush();
		}

		$query = $this->db->getQueryBuilder();
		$query->select('*')
			->from('ssepush_config')
			->where($query->expr()->eq('parameter', $query->createNamedParameter("keepalive")));
		$result = $query->execute();
		$keepalive = "300";
		if ($row = $result->fetch()){
			$keepalive = $row['value'];
		}
		
		while (true){
			foreach($keys as $key) $redis->expire('auth_'.$key, 600+intval($keepalive));
			$element = $redis->brPop($keys, intval($keepalive));
			if ($element !== false && is_array($element) && array_key_exists(1, $element)){
				if (strpos($element[1], 'shutdown') === 0){
					if (getmypid() != explode("_", $element[1])[1]) return(0);
					else continue;
				}
				echo "event: notification".PHP_EOL;
				echo "data: ".$element[1].PHP_EOL;
				echo PHP_EOL;
				flush();
			} else {
				echo "event: ping".PHP_EOL;
				echo "data: timer".PHP_EOL;
				echo PHP_EOL;
				flush();
			}
		}
	}

	/**
	 * Authorize notify and SSE subscription connections.
	 *
	 * @NoCSRFRequired
	 * @NoAdminRequired
	 *
	 * @param string $deviceIdentifiers
	 * @param string $pushTokenHash
	 * @return JsonResponse
	 */
	public function authorize(string $deviceIdentifiers, string $pushTokenHash){
		/* The way authorizations are implemented, is that the client device
		 * must register all the $deviceIdentifiers and the $pushTokenHash
		 * with this function, which is secured by nextcloud authentication.
		 *
		 * This function creates keys in redis with fairly short term expire
		 * set. When the client registers for SSE or when the Nextcloud
		 * server(s) connect for notifications, redis will be checked for
		 * authorization keys matching those provided on the connection.
		 *
		 * The expiration on the keys will be updated each iteration of the
		 * SSE main loop. This allows the authentication to be maintained
		 * for as long as the client continues to use it, but then expires
		 * it shortly after.
		 */

		$redis = $this->redis_connect();

		$deviceIdentifiers = explode(",", $deviceIdentifiers);
		foreach($deviceIdentifiers as $value){
			$redis->setEx('auth_'.$pushTokenHash.'_'.$value, 600, $this->userSession->getUser()->getUID());
		}
		return new JSONResponse(['success' => true]);
	}

	/**
	 * Receive notifications from 3rd parties.
	 *
	 * @PublicPage
	 * @NoCSRFRequired
	 *
	 * @param string $auth
	 * @param string $id
	 * @param string $subject
	 * @param string $message
	 * @param string|null $type		optional
	 * @param string|null $sparams		optional
	 * @param string|null $mparams		optional
	 * @param string|null $link		optional
	 * @param array|null $action		optional
	 *
	 * @return JsonResponse
	 */
	public function external(string $auth, string $id, string $subject,
		string $message, string $type=null, string $sparams=null,
		string $mparams=null, string $link=null, array $action=null){

		$manager = \OC::$server->get(\OCP\Notification\IManager::class);
		$notification = $manager->createNotification();

		$query = $this->db->getQueryBuilder();
		$query->select('*')
			->from('ssepush_tokens')
			->where($query->expr()->eq('token', $query->createNamedParameter($auth)));
		$result = $query->execute();
		$service = null;
		$userid = null;
		if ($row = $result->fetch()){
			$service = $row['ext_name'];
			$userid = $row['user_id'];
		}
		if ($service === null) return new JSONResponse(['success' => false], Http::STATUS_UNAUTHORIZED);

		$subject_params = $sparams == null ? array() : json_decode($sparams, true);
		$message_params = $mparams == null ? array() : json_decode($mparams, true);

		$notification->setApp('ssepushx-'.$service)
			->setUser($userid)
			->setDateTime(new \DateTime())
			->setObject($type == null ? "ext" : $type, $id)
			->setSubject($subject, $subject_params)
			->setMessage($message, $message_params);
		if ($link !== null) $notification->setLink($link);

		if ($action != null) foreach($action as $mact){
			$mact = json_decode($mact, true);
			$act = $notification->createAction();
			$act->setLabel($mact['label']);
			$act->setLink($mact['link'], $mact['method']);
			$notification->addAction($act);
		}

		$manager->notify($notification);

		return new JSONResponse(['success' => true]);
	}

	/**
         * Create an authorization token for a new 3rd party service.
         *
         * @NoAdminRequired
         * @NoCSRFRequired
         *
         * @param string $serv_name
         *
         * @return JsonResponse
         */
	public function genauth(string $serv_name){
		$token = openssl_random_pseudo_bytes(64);
		if ($token !== false){
			error_log("user: ".$this->userSession->getUser()->getUID().", serv_name: ".$serv_name.", token: ".bin2hex($token));
			$query = $this->db->getQueryBuilder();
			$query->insert('ssepush_tokens')
				->values([
					'user_id' => $query->createNamedParameter($this->userSession->getUser()->getUID()),
					'ext_name' => $query->createNamedParameter($serv_name),
					'token' => $query->createNamedParameter(bin2hex($token)),
				]);
			$query->execute();
			return new JSONResponse(['success' => true]);
		}
		return new JSONResponse(['success' => false]);
	}

	/**
	 * Delete an authorization token.
	 *
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @param string $token
	 *
	 * @return JsonResponse
	 */
	public function rmauth(string $token){
		$query = $this->db->getQueryBuilder();
		$query->delete('ssepush_tokens')
			->where($query->expr()->eq('token', $query->createNamedParameter($token)))
			->andWhere($query->expr()->eq('user_id', $query->createNamedParameter($this->userSession->getUser()->getUID())));
		$query->execute();
		return new JSONResponse(['success' => true]);
	}

	/**
	 * Set keepalive interval.
	 *
	 * @NoCSRFRequired
	 *
	 * @param string $value
	 *
	 * @return JsonResponse
	 */
	public function setkeepalive(string $value){
		$query = $this->db->getQueryBuilder();
		$query->delete('ssepush_config')
			->where($query->expr()->eq('parameter', $query->createNamedParameter("keepalive")));
		$query->execute();
		$query = $this->db->getQueryBuilder();
		$query->insert('ssepush_config')
			->values([
				'parameter' => $query->createNamedParameter("keepalive"),
				'value' => $query->createNamedParameter($value),
			]);
		$query->execute();
		return new JSONResponse(['success' => true]);
	}
}
