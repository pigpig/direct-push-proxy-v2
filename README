v2 of Direct-Push Proxy for Nextcloud

This version differs from the original version in that it
is "more or less" installable as a Nextcloud plugin.

Installation:

Go into nextcloud/apps/ and run;

sudo -u apache git clone https://gitlab.com/Nextcloud-Push/direct-push-proxy-v2 ssepush
(change apache to the appropriate user account if it differs on your distro)

Go into your nextcloud instance (web browser), User icon --> Apps
Scroll to the bottom of "Your apps" and find "SSE Push Notifications" and press the
enable button.

Now go into the Android application from https://gitlab.com/Nextcloud-Push/nextcloud-push-notifier

Add all of the nextcloud accounts from which you want to receive notifications.
The LAST account added will be designated as the "master" and marked with a '*'.

Only the master instance requires the push proxy to be installed.

When all accounts have been added, press the "load service" button.


Possible issues;
1) Proxy timeout. If you observe the application reconnecting every 60 seconds, your
webserver is probably timing out. Add the following line to your Apache config, or
the equivalent for your webserver if not Apache;
ProxyTimeout 600

2) Messages are sent to the proxy, but don't seem to be delivered to the mobile, or
are delivered with a delay up to several minutes. This is because there is buffering
happening somewhere in your webserver/php stack. For Apache, you may add the following
to your php configuration;
<Proxy "fcgi://localhost/" disablereuse=on flushpackets=on max=10>
    </Proxy>

In particular, the "flushpackets=on" directive tells php-fpm not to buffer.
